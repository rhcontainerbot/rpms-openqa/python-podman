%global python3_pkgversion 3

%global pypi_name podman
%global desc %{pypi_name} is a library of bindings to use the RESTful API for Podman.

%global pypi_dist 4
%global built_tag_strip 5.4.0.1

Name: python-%{pypi_name}
Epoch: 3
Version: %{built_tag_strip}
Packager: Podman Debbuild Maintainers <https://github.com/orgs/containers/teams/podman-debbuild-maintainers>
License: ASL-2.0
Release: 0%{?dist}
Summary: RESTful API for Podman
URL: https://github.com/containers/%{pypi_name}-py
Source0: %{url}/releases/download/v%{built_tag_strip}/%{pypi_name}-%{version}.tar.gz
BuildArch: noarch

%description
%desc

%package -n python%{python3_pkgversion}-%{pypi_name}
%if "%{_vendor}" == "debbuild"
BuildRequires: git
BuildRequires: python%{python3_pkgversion}-dev
BuildRequires: python%{python3_pkgversion}-toml
BuildRequires: python%{python3_pkgversion}-requests
BuildRequires: python%{python3_pkgversion}-setuptools
BuildRequires: python%{python3_pkgversion}-xdg
Requires: python%{python3_pkgversion}-toml
Requires: python%{python3_pkgversion}-requests
Requires: python%{python3_pkgversion}-xdg
%endif
Provides: %{pypi_name}-py = %{version}-%{release}
Provides: python%{python3_pkgversion}dist(%{pypi_name}) = %{pypi_dist}
Provides: python%{python3_version}dist(%{pypi_name}) = %{pypi_dist}
Obsoletes: python%{python3_pkgversion}-%{pypi_name}-api <= 0.0.0-1
Provides: python%{python3_pkgversion}-%{pypi_name}-api = %{epoch}:%{version}-%{release}
Summary: %{summary}
%{?python_provide:%python_provide python%{python3_pkgversion}-%{pypi_name}}

%description -n python%{python3_pkgversion}-%{pypi_name}
%desc

%prep
%autosetup -Sgit -n %{pypi_name}-%{built_tag_strip}

%if 0%{?fedora} || 0%{?rhel} >= 9
%generate_buildrequires
%pyproject_buildrequires %{?with_tests:-t}
%endif

%build
export PBR_VERSION="0.0.0"
python3 setup.py sdist bdist

%install
export PBR_VERSION="0.0.0"
python3 setup.py install --root %{buildroot}

%files -n python%{python3_pkgversion}-%{pypi_name}
%license LICENSE
%doc README.md
%{python3_sitelib}/podman/*
%{python3_sitelib}/podman-*/*

%changelog
